package com.queue.queue.controller;

import com.queue.queue.model.Transaction;
import com.queue.queue.rabbitmq.RabbitTransactionReceiverService;
import com.queue.queue.rabbitmq.RabbitTransactionSenderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TransactionController {

    @Autowired
    RabbitTransactionSenderService rabbit;

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/transactions")
    Transaction postTransaction(@RequestBody Transaction transaction) {
rabbit.sendTransaction(transaction);
        return transaction;
    }
}
