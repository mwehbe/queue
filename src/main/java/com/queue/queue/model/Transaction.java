package com.queue.queue.model;

public class Transaction {
    private final int amount;
    private final String currency;

    public Transaction() {
        this.amount = 0;
        this.currency = null;
    }

    public Transaction(int amount, String currency) {
        this.amount = amount;
        this.currency = currency;
    }

    public int getAmount() {
        return amount;
    }

    public String getCurrency() {
        return currency;
    }
}
