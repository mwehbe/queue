package com.queue.queue.rabbitmq;

import com.queue.queue.model.Transaction;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RabbitTransactionReceiverService {
    private RabbitTemplate rabbit;
    private MessageConverter converter;

    @Autowired
    public RabbitTransactionReceiverService(RabbitTemplate rabbit) {
        this.rabbit = rabbit;
        this.converter = rabbit.getMessageConverter();
    }

    public Transaction receiveTransaction() {
        Message message = rabbit.receive("ozan.transactions",2000);
        return message != null
                ? (Transaction) converter.fromMessage(message)
                : null;
    }
}
