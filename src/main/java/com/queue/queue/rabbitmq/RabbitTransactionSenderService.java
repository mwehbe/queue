package com.queue.queue.rabbitmq;

import com.queue.queue.model.Transaction;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RabbitTransactionSenderService {
    private RabbitTemplate rabbit;

    @Autowired
    public RabbitTransactionSenderService(RabbitTemplate rabbit) {
        this.rabbit = rabbit;
    }
    public void sendTransaction(Transaction transaction) {
        MessageConverter converter = rabbit.getMessageConverter();
        MessageProperties props = new MessageProperties();
        Message message = converter.toMessage(transaction, props);
        rabbit.send("ozan.transactions", message);
    }
}
