package com.queue.queue.Listener;

import com.queue.queue.model.Transaction;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
public class TransactionListenerPrint {
    @RabbitListener(queues = "ozan.transactions")
    public void receiveOrder(Transaction transaction) {
        System.out.println("Print only "+ transaction.getAmount()+ " " +transaction.getCurrency());
    }
}
