package com.queue.queue.Listener;

import com.queue.queue.model.Transaction;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
public class TransactionListenerPrintHi {
    @RabbitListener(queues = "ozan.transactions")
    public void receiveOrder(Transaction transaction) {
        System.out.println("print with hi "+transaction.getAmount()+ transaction.getCurrency());
    }
}
